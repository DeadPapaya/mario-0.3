package mario.utils;

import javafx.scene.Scene;
import mario.screen.Screen;

public final class ScreenController {

    private static final Scene scene = new Scene(Screen.MENU.getScreen());

    private ScreenController() {
    }

    public static void activateScreen(Screen screen) {
        scene.setRoot(screen.getScreen());
    }

    public static Scene getScene() {
        return scene;
    }
}
