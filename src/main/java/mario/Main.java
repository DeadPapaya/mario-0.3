package mario;

import javafx.application.Application;
import javafx.stage.Stage;
import mario.utils.ScreenController;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(ScreenController.getScene());
        primaryStage.show();
    }
}
