package mario.screen;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import mario.utils.ScreenController;

public class SettingsScreen {

    @FXML
    private void saveSettings(ActionEvent actionEvent) {
        ScreenController.activateScreen(Screen.MENU);
    }
}
