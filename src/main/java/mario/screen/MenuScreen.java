package mario.screen;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import mario.utils.ScreenController;

public class MenuScreen {

    @FXML
    private void openSettings(ActionEvent actionEvent) {
        ScreenController.activateScreen(Screen.SETTINGS);
    }
}
