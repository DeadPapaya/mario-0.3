package mario.screen;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public enum Screen {

    MENU("menu_screen.fxml"),
    SETTINGS("settings_screen.fxml");

    private Parent screen;

    Screen(String resource) {
        try {
            screen = FXMLLoader.load(getClass().getResource(resource));
        } catch (IOException e) {
            // todo logger
            e.printStackTrace();
        }
    }

    public Parent getScreen() {
        return screen;
    }
}
